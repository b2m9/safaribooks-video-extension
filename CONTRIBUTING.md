# Contributing

Contributions are always welcome! Thank you for taking the time! 🎉 🎉

Since this is a very small extension, it's also super easy to contribute. There
are 3 ways to contribute: bug repots, feature requests and coding.

### Reporting issues

Oh snap, you've found a bug? Well, let's deal with it then. Here are the steps:

1. Head over to the [issue tracker](https://gitlab.com/b2m9/safaribooks-video-extension/issues)
and see if a similar issue already exists. If it does, then feel free to
contribute to the discussion over there.
2. If there isn't a similar issue, then please [create a new issue](https://gitlab.com/b2m9/safaribooks-video-extension/issues/new).
Be as detailed as possible, it will help to reproduce the bug.
3. Submit the issue and frequently check back to see if there are any follow-up
questions. It would be a bummer if the issue can't be reproduced because that.

Thank you for bug hunting! 🐞

### Feature Proposal

Got a nice idea? Great, then follow these steps:

1. Check out the [issue tracker](https://gitlab.com/b2m9/safaribooks-video-extension/issues)
to see if there's a similar suggestion. If yes, join the discussion and share
your view.
2. If there is no such feature proposal, [create a new issue](https://gitlab.com/b2m9/safaribooks-video-extension/issues/new).
If you have supplement material (like screenshots), feel free to attach it to
your proposal.
3. Hit the submit button and stay engaged in the coming discussion. 

Thank you for suggesting a new feature! 🌈

### Development

Want to get your hands dirty? That's not too difficult.

Simply [create a Merge Request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html)
and you'll receive comments within a few days. In case you're not familiar
with Gitlab's concept of Merge Requests (or MR), then think of it as an
equivalent to Github's Pull Requests.
[Here's more information on Merge Requests](https://docs.gitlab.com/ee/user/project/merge_requests/index.html).

To increase chances that your MR will be merged, here are 3 things:

1. Here are the browsers that you need to test for:
    - Chrome and [Chrome Canary](https://www.google.com/chrome/browser/canary.html)
    - Opera and [Opera Beta](http://www.opera.com/computer/beta)
    - Firefox and [Firefox Developer Edition](https://www.mozilla.org/en-US/firefox/developer/)
2. [Stick to StandardJS as a style guide](https://standardjs.com/#how-do-i-contribute-to-standard)
3. [Write a good commit message in Git](http://tbaggery.com/2008/04/19/a-note-about-git-commit-messages.html)

Thank you for contributing! You're now officially even more awesome! 🙌

