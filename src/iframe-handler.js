/* Delete video control elements:
 *
 * Safaribooksonline.com is using an iframe for its video player widget.
 * The following small script will delete the respective DOM elements in the
 * inline frame.
 *
 * More information about the widget can be found here:
 * http://player.kaltura.com/docs/kwidget
 */
const injectedScript = function () {
  const pushState = window.history.pushState
  const isNewDesign = document.getElementById('root') !== null
  const isOldDesign = document.querySelector('body.video') !== null

  const IFRAME_ID = 'js-kaltura-player-region_ifp'
  const TITLE_SELECTOR = '.topBarContainer'
  const ICON_SELECTOR = '.icon-play'
  const INTERVAL_TIME = 250
  const DOM_TIMEOUT = 1250
  let widgetIntervalId = null
  let elementIntervalId = null
  let titleElement = null
  let iconElement = null
  let isCallbackAttached = null

  /**
   * Modify history.pushState:
   *
   * - Override built-in function to allow a `onpushstate` handler
   * - Original `pushState` function will be executed as usual
   */
  window.history.pushState = function () {
    // Execute attached event handler, if any
    if (typeof window.history.onpushstate === 'function') {
      window.history.onpushstate()
    }
    // Execute original pushState function
    return pushState.apply(window.history, arguments)
  }

  /**
   * Whether new or old design, the goal is to attach an event handler to
   * `window.kWidget`. This event will be fired when a new video is loaded
   * via Kaltura's widget and ready in DOM. At this point the callback
   * `videoCallbackFn` will be executed to remove those overlays from DOM.
   */
  if (isNewDesign) {
    kWidgetSearch()

    window.onpopstate = window.history.onpushstate = function () {
      /**
       * A timeout is necessary, because React fires a pop or push event
       * before the actual DOM is changed. The side effect is that the callback
       * might deal with the DOM of the old page just before it is replaced
       * by the content of the new page.
       */
      setTimeout(kWidgetSearch, DOM_TIMEOUT)
    }
  }

  if (isOldDesign) {
    window.kWidget.addReadyCallback(videoCallbackFn)
  }

  /**
   * If a callback hasn't been attached to `window.kWidget` and a video is
   * detected on the current page, wait until the lazy-loaded kWidget is ready
   * and attach callback to the video widget.
   */
  function kWidgetSearch () {
    /**
     * If callback is already attached to `window.kWidget`, then delete
     * `onpopstate` and `onpushstate` event handlers to stop adding more
     * callbacks to `window.kWidget`.
     */
    if (isCallbackAttached) {
      window.onpopstate = window.history.onpushstate = null
      return null
    }

    /**
     * Detect whether DOM has a video container or a temporary placeholder
     * for a video.
     */
    let videoContainer = document.querySelectorAll('article > .container')
    let hasVideo = false

    if (videoContainer.length > 0) {
      hasVideo = (videoContainer[0].children[1]
        .className.search(/VideoPlayer/) > -1) || (videoContainer[0]
        .children[1].className.search(/videoPlaceholder/) > -1)
    } else {
      console.log('no video found')
    }

    /**
     * If a video is detected, trigger intervalFn to find lazy-loaded
     * `window.kWidget`. Once a callbackFn has been attached, the interval
     * will be cleared and the flag `isCallbackAttached` set to be true to
     * avoid adding multiple callbacks.
     */
    if (hasVideo) {
      widgetIntervalId = setInterval(function () {
        if (window.kWidget) {
          window.kWidget.addReadyCallback(videoCallbackFn)
          isCallbackAttached = true
          clearInterval(widgetIntervalId)
        }
      }, INTERVAL_TIME)
    }
  }

  function videoCallbackFn () {
    let frame = document.getElementById(IFRAME_ID)

    /**
     * Interval function to find respective DOM elements and remove them
     *
     * - Not the prettiest solution, but it is the most robust one
     * - In some browsers under some circumstances, title bar and play icon
     *   are not ready when `onload` of the iFrame is executed
     * - When both elements are found, then remove those elements,
     *   clear interval and reset element vars
     */
    elementIntervalId = setInterval(function () {
      titleElement = titleElement || getOverlayById(frame.contentDocument, TITLE_SELECTOR)
      iconElement = iconElement || getOverlayById(frame.contentDocument, ICON_SELECTOR)

      if (titleElement && iconElement) {
        titleElement.remove()
        iconElement.remove()

        logSuccess()
        clearInterval(elementIntervalId)

        titleElement = null
        iconElement = null
      }
    }, INTERVAL_TIME)
  }

  function getOverlayById (doc, id) {
    return doc.querySelector(id)
  }

  function logSuccess () {
    console.log('Safari Video Cleaner extension: ' +
      'Removal of video player overlays successful.')
  }
}

/**
 * Inject script to DOM, because a normal WebExtension content script cannot
 * override built-in functionality like `window.history.pushState`.
 */
let script = document.createElement('script')
script.textContent = '(' + injectedScript + ')();'
document.body.appendChild(script)
script.remove()
