_Describe the bug you want to report here in a detailed manner._

Checklist before submitting the bug report:

- [ ] Accurate and helpful title
- [ ] Detailed bug report (answer what and when)
- [ ] Mention browser, browser version and operating system
- [ ] Temporarily deactivated other browser extensions to rule out side effects
- [ ] Attach information from your browser's debug console, if any
- [ ] Attach additional screenshots to illustrate the bug, if any
