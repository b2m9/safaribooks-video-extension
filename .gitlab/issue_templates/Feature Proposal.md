_Describe your feature proposal here in a detailed manner._

Checklist before submitting the feature proposal:

- [ ] Accurate and helpful title
- [ ] Detailed feature proposal (answer what and why)
- [ ] Attach additional material to illustrate your proposal, if any
