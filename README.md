# Extension is deprecated

This extension is no longer maintained and not compatible with the current (as of 2022) interface of O'Reilly/Safari. Thank you for using it, but it's time to remove it from your browser.

---

🔥 Download here for

__[Firefox](https://addons.mozilla.org/en-US/firefox/addon/video-cleaner-for-safari/)__ —
__[Opera](https://addons.opera.com/en/extensions/details/video-cleaner-for-safaritm/)__ —
__[Chrome](https://chrome.google.com/webstore/detail/video-cleaner-for-safari/ijocmobeifedpdihdjggkpacbhmjooig)__

<img width="640" alt="Before and after installing Safari Video Cleaner extension" src="media/chrome/chrome-1.jpg">

# Be more productive on safaribooksonline.com

Video Cleaner for Safari is a lightweight extension that will improve your
learning experience on O'Reilly's education platform [Safari](https://www.safaribooksonline.com)
by cleaning the video player interface of any obstructions, when you pause a
lecture or code along in your editor.

## Why do I need this?

[Safari](https://www.safaribooksonline.com) has a rapidly growing catalogue of
video lectures. However, problems occur when you pause a video or start having
multiple windows side by side – for example when you code along to the lectures
in your editor.

When you pause a video, redundant interface elements obstruct your view of the
video – rather unpleasant when you try to read the source code underneath. At
smaller window sizes other interface elements start to hover above the video
player, even when it is playing – also a rather suboptimal learning experience.

Video Cleaner for Safari will remove those obstacles and improve your overall
learning experience on Safari.

<img width="640" alt="Multiple apps side by side are not a problem anymore" src="media/chrome/chrome-2.jpg">

## What does it do?

Video Cleaner for Safari is only activate when you visit
[Safari](https://www.safaribooksonline.com), and even then it just runs
quietly in your background. The extension itself does two things when it
detects a video on Safari.

First, it injects a tiny CSS files to fix a faulty media query. This is
preventing interface elements from hovering above your video player at a
certain window size. Second, it injects a small JS script to remove redundant
elements of the video player widget when you pause a video.

## Is it compatible with Safari's upcoming redesign?

If you refer to [Safari's case studies](https://www.safaribooksonline.com/case-studies/),
then the answer is yes. It is expected that O'Reilly will roll out this design
to the entire Safari platform in the future. It is likely that this new
React-based frontend will break the extension frequently as long as it is
still under development. Please provide feedback and I'll fix any issues as
soon as possible.

## Support, feedback and contact

In case you need support, please [create a new issue](https://gitlab.com/b2m9/safaribooks-video-extension/issues/new).
If you don't have a Gitlab account, feel free to drop me an email at
`support@b2m9.com`. Any kind of feedback is appreciated, since I'm always
looking for ways to improve this little extension.

## Fine print

Obviously, all product and company names are trademarks or registered
trademarks of their respective holders. I am not affiliated to any of them.
